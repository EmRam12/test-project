package ui;

/**
 * Main application to execute the Window class
 */
public class App {
     public static void main(String[] args) {
        Window window = new Window();
        window.setVisible(true);      
        window.setResizable(false);
    }
}
