package ui;

import javax.swing.*;
import java.awt.*;

/**
 * Creates a template for the back button
 */
public class ButtonBack extends JButton{

    public ButtonBack(String text){
        super(text);
        Font buttonFont = new Font("Helvetica", Font.PLAIN, 25);
        this.setFont(buttonFont);
        this.setBorder(null);
        Color colorButtons = new Color(64, 64, 64);
        this.setBackground(colorButtons);
        this.setForeground(Color.WHITE);
    }
}
